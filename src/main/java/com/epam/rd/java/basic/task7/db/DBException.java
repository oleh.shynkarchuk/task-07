package com.epam.rd.java.basic.task7.db;

import java.util.logging.Logger;

public class DBException extends Exception {
	private static final Logger LOGGER = Logger.getLogger(DBException.class.getName());
	public DBException(String message, Throwable cause) {
	LOGGER.severe(message);
	}

}
