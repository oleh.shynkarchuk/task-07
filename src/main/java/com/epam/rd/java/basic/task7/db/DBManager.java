package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String URL= "jdbc:mysql://localhost:3306/test2db";
	private static final String USER="root";
	private static final String PASSWORD="qwerty00QWERTY00";
	private static final String FULL_URL=URL+"?"+"user="+USER+"&password="+PASSWORD;
	private static final String INSERT_USER="INSERT INTO users(login)VALUES(?)";
	private static final String INSERT_TEAM="INSERT INTO teams (name)VALUES(?)";
	private static final String DELETE_USER="DELETE FROM users WHERE login=?";
	private static final String DELETE_TEAM="DELETE FROM teams WHERE name=?";
	private static final String UPDATE_TEAM="UPDATE teams SET name =? WHERE id =?";
	private static final String INSERT_USER_TEAMS="INSERT INTO users_teams (user_id,team_id) VALUES (?,?)";
	private static final String GET_USER_TEAMS="SELECT * FROM users_teams WHERE user_id=?";

	private static DBManager instance;


	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public Connection getConnection(String connectionUrl) throws DBException {
		try(FileInputStream fio = new FileInputStream("app.properties")){
			Properties properties = new Properties();
			properties.load(fio);
			return DriverManager.getConnection(properties.getProperty(connectionUrl));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList= new ArrayList<>();
		try(Connection con = getConnection("connection.url");
			Statement stmt= con.createStatement();
			ResultSet rs=stmt.executeQuery("SELECT * FROM users u order by id")){
			while (rs.next()){
				User user=new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				userList.add(user);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
		try {
			con=getConnection("connection.url");
			con.setAutoCommit(false);
			stmt=con.prepareStatement(INSERT_USER);
			//stmt.setLong(1,user.getId());
			stmt.setString(1,user.getLogin());
			stmt.executeUpdate();
			con.commit();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			rollBack(con);
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}finally {
			close(stmt);
			close(con);
		}
		return false;
	}

	private void rollBack(Connection con) {
		try {
			con.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void close(AutoCloseable stmt) {
		if (stmt != null){
		try {
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			}
		}
	}
	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
		for (User user:users) {
			try {
			con=getConnection("connection.url");
			stmt=con.prepareStatement(DELETE_USER);
			stmt.setString(1,user.getLogin());
			stmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
				throw new DBException(throwables.getMessage(),throwables.getCause());
		}finally {
			close(stmt);
			close(con);
			}
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		User user=new User();
		try(Connection con = getConnection("connection.url");
			PreparedStatement stmt= con.prepareStatement("SELECT * FROM users WHERE login=?")){
			stmt.setString(1,login);
			try (ResultSet rs=stmt.executeQuery()) {
				while (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team=new Team();
		try(Connection con = getConnection("connection.url");
			PreparedStatement stmt= con.prepareStatement("SELECT * FROM teams WHERE name=?")){
			stmt.setString(1,name);
			try (ResultSet rs=stmt.executeQuery();) {
				while (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList= new ArrayList<>();
		try(Connection con = getConnection("connection.url");
			Statement stmt= con.createStatement();
			ResultSet rs=stmt.executeQuery("SELECT * FROM teams t order by id")){
			while (rs.next()){
				Team team=new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teamList.add(team);
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}
		return teamList;
	}

	private Team getTeamById(int id) throws DBException {
		Team team=new Team();
		try(Connection con = getConnection("connection.url");
			PreparedStatement stmt= con.prepareStatement("SELECT * FROM teams WHERE id=?")){
			stmt.setLong(1,id);
			try (ResultSet rs=stmt.executeQuery();) {
				while (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}
		return team;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
		try {
			con=getConnection("connection.url");
			stmt=con.prepareStatement(INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,team.getName());
			int count=stmt.executeUpdate();
			if (count >0) {
				try (ResultSet rs=stmt.getGeneratedKeys()){
					if(rs.next()) team.setId(rs.getInt(1));
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}finally {
			close(stmt);
			close(con);
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
			try {
				con = getConnection("connection.url");
				con.setAutoCommit(false);
				stmt = con.prepareStatement(INSERT_USER_TEAMS);
				stmt.setLong(1, getUser(user.getLogin()).getId());
				for (Team team:teams) {
					stmt.setLong(2, team.getId());
					stmt.executeUpdate();
				}
				con.commit();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
				if (con != null) {
					rollBack(con);
				}
				throw new DBException(throwables.getMessage(),throwables.getCause());
			}finally {
				close(con);
				close(stmt);
			}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team>teamList=new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt=null;
			try {
				con = getConnection("connection.url");
				stmt = con.prepareStatement(GET_USER_TEAMS);
				stmt.setLong(1, getUser(user.getLogin()).getId());
				ResultSet rs=stmt.executeQuery();
				while (rs.next()){
					teamList.add(getTeamById(rs.getInt("team_id")));
				}
			} catch (SQLException throwables) {
				throwables.printStackTrace();
				throw new DBException(throwables.getMessage(),throwables.getCause());
			}finally {
				close(con);
				close(stmt);
			}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
		try {
			con=getConnection("connection.url");
			stmt=con.prepareStatement(DELETE_TEAM);
			stmt.setString(1,team.getName());
			stmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}finally {
			close(stmt);
			close(con);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt=null;
		try {
			con=getConnection("connection.url");
			stmt=con.prepareStatement(UPDATE_TEAM);
			stmt.setString(1,team.getName());
			stmt.setLong(2,team.getId());
			stmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			throw new DBException(throwables.getMessage(),throwables.getCause());
		}finally {
			close(stmt);
			close(con);
		}
		return false;
	}
}
